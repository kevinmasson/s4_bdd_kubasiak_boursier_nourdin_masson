SI2
PEter Boursier
Maxime Kubasiak
Valentin Nourdin
Kevin Masson

Depot : https://bitbucket.org/kmasson/s4_bdd_kubasiak_boursier_nourdin_masson



TD4 : Pour les associations, Eloquent fait une première requète pour obtenir tous les tuples, puis, lors du parcours, execute un requète supplementaire par tuple pour obtenir le resultat attendu

TD5 : 
    -L'indexation permet un recherche plus performante dans la table : on ne fait plus un parcours séquentiel de chaque tuple. L'indexation crée en effet un arbre, plus rapide à parcourir

    - On peut créer un index sur la colomne souhaitée
    - Le résultant est beaucoup moins probant car le nombre de données stockées dans cette column est bien plus faible


TD1 à 6 : terminé
TD7 : en cours


SQL à executer :

CREATE TABLE IF NOT EXISTS `games`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `mail` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  `password` VARCHAR(255) NULL,
  `tel` VARCHAR(20) NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `games`.`com`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `games`.`com` (
  `com_id` INT NOT NULL AUTO_INCREMENT,
  `com` VARCHAR(1400) NULL,
  `user_id` INT NULL,
  `game_id` INT NULL,
  PRIMARY KEY (`com_id`))
