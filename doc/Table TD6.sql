drop table if exists `games`.`user`;
drop table if exists `games`.`com`;

CREATE TABLE IF NOT EXISTS `games`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `mail` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  `password` VARCHAR(255) NULL,
  `tel` VARCHAR(20) NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `games`.`com`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `games`.`com` (
  `com_id` INT NOT NULL AUTO_INCREMENT,
  `com` VARCHAR(1400) NULL,
  `user_id` INT NULL,
  `game_id` INT NULL,
  PRIMARY KEY (`com_id`))
ENGINE = InnoDB;