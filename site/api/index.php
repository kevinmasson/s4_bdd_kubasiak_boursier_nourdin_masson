<?php


require_once('../vendor/autoload.php');
use conf\DbConf;
use gamepedia\Rest;

DbConf::init('../conf/db.gamepedia.conf.ini');

$app = new \Slim\Slim([
    'templates.path' => 'includes',
    'debug' => true
]);

$app->map('/', function () use ($app) {
    Rest::home();
})->name('home')->via(array("GET", "POST"));

$app->map('/games', function () use ($app) {
    Rest::showGames($app->request->get('page'));
})->via(array("GET", "POST"));

$app->map('/game/:id', function ($id) use ($app) {

    Rest::showGame($id);
})->via(array("GET", "POST"))->name('oneGame');

$app->map('/game/:id/comments', function($id) use ($app){
  Rest::showGameComment($id);
})->via(array("GET", "POST"))->name('oneGameComments');

$app->map('/game/:id/characters', function($id) use ($app){
  Rest::showGameCharacters($id);
})->via(array("GET", "POST"))->name('oneGameCharacters');

$app->run();

?>
