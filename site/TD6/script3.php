<?php

require_once('../vendor/autoload.php');


use conf\DbConf;
use gamepedia\models\Com;

DbConf::init('../conf/db.gamepedia.conf.ini');


echo "TD6 <br>";

echo "Creer 250 000 commentaires<br>";


$t = new gamepedia\Time();

$faker = Faker\Factory::create('fr_FR');

for ($i = 0; $i < 250000; $i++) {
    $c = new Com();
    $c->com = $faker->text;
    $c->game_id = rand(1, 20000);
    $c->user_id = rand(1, 5000);
    $c->save();
}

$t->end();
