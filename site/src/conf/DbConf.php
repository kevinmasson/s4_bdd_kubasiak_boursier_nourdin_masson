<?php

namespace conf;

use Illuminate\Database\Capsule\Manager;

class DbConf
{

    public static function init($fichier)
    {
        $db = new Manager();
        $db->addConnection(parse_ini_file($fichier));
        $db->setAsGlobal();
        $db->bootEloquent();
    }
}