<?php

namespace gamepedia;

use Illuminate\Database\Capsule\Manager as DB;

class Time
{
    private $startTime, $reponseTime;

    public function __construct()
    {
        $this->startTime = microtime(true);
        DB::enableQueryLog();

    }

    public function end()
    {
        $this->reponseTime = microtime(true) - $this->startTime;
        $this->display();
    }

    public function display()
    {
        $html = '<div style="z-index:999;position:fixed;bottom:0;right:0;padding:20px;background-color:rgba(255,0,0,.5);color:white;">';
        $html .= '<span>Temps de réponse : ' . ($this->reponseTime) . ' seconds</span>';
        $html .= '</div>';
        echo $html;

        $query = DB::getQueryLog();
        $nb = count($query);
        echo "<h2>Log req ($nb en tout)</h2>";
        echo "<hr>";
        foreach ($query as $key => $value) {
            echo "<h4>Requette $key</h4>";
            echo "<p>" . $value["query"] . "<p>";
            echo "<hr>";
        }
        echo "<hr>";
    }

}
