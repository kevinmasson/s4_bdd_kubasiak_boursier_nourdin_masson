<?php

namespace gamepedia;

use gamepedia\models\Game;
use gamepedia\models\Com;
use gamepedia\models\Character;

class Rest
{
    public static function home()
    {
        echo "Bienvenue sur notre api";
    }

    public static function showGame($id)
    {
        $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
        $links = ["comments" => ["href" => $app->urlFor("oneGameComments", array('id' => $id))],
                  "characters" => ["href" => $app->urlFor("oneGameCharacters", array('id' => $id))]];
        $res = ['game' => Game::where('id', '=', $id)->first(['id', 'name', 'alias', 'deck', 'original_release_date', 'description']), 'links' => $links];
        echo json_encode($res);
    }

    public static function showGames($numPage = 1)
    {
        $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
        $nbGames = Game::count();
        $nbPages = (int)($nbGames / 200);

        if ($numPage < 1) $numPage = 1;
        else if ($numPage > $nbPages) $numPage = $nbPages;

        $links = [];
        if ($numPage > 1)
            $links['prev'] = json_encode(['href' => '/api/games?page=' . ($numPage - 1)]);
        if ($numPage < $nbPages)
            $links['next'] = json_encode(['href' => '/api/games?page=' . ($numPage + 1)]);

        $listeJeux = array();
        $games = Game::take(200)->skip(($numPage - 1) * 200)->get(['id', 'name', 'alias', 'deck']);
        foreach ($games as $g) {
          $links = ["comments" => ["href" => $app->urlFor("oneGameComments", array('id' => $g->id))],
                    "characters" => ["href" => $app->urlFor("oneGameCharacters", array('id' => $g->id))]];
            $listeJeux[$g->id] = ["game" => $g, "links" => $links];
        }
        $res = ['games' => $listeJeux, 'links' => $links];
        echo json_encode($res);
    }

    public static function showGameComment($id){
      $app = \Slim\Slim::getInstance();
      $app->response->headers->set('Content-Type', 'application/json');
      $lien = $app->urlFor("oneGameComments", array('id'=>$id) );
      $links = ["comments" => ["href" => $app->urlFor("oneGameComments", array('id' => $id))],
                "characters" => ["href" => $app->urlFor("oneGameCharacters", array('id' => $id))]];
      $g = Game::where('id', '=', $id)->first(['id', 'name', 'deck']);
      /*$com = Com::with(['jeux' => function($query) use($id){
        $query->where('id', '=', $id);
      }])->get();*/
        $com = $g->commentaires;
      $res = ['game'=> $g, 'comments'=> $com, 'links' => $links];
      echo json_encode($res);
    }

    public static function showGameCharacters($id){
      $app = \Slim\Slim::getInstance();
      $app->response->headers->set('Content-Type', 'application/json');
      $links = ["comments" => ["href" => $app->urlFor("oneGameComments", array('id' => $id))],
                "characters" => ["href" => $app->urlFor("oneGameCharacters", array('id' => $id))]];
      $g = Game::where('id', '=', $id)->first(['id', 'name', 'deck']);
      $car =  $g->personnages()->get(['name','real_name','id']);
      $res = ['game'=> $g, 'charachters'=> $car, 'links' => $links];
      echo json_encode($res);
    }
}
