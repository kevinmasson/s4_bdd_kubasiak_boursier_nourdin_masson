<?php

namespace gamepedia\models;

class User extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'user';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    public function commentaires()
    {
        return $this->hasMany('gamepedia\models\Com', 'user_id');
    }

}
