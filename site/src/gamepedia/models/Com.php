<?php

namespace gamepedia\models;

class Com extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'com';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function utilisateur()
    {
        return $this->belongsTo('gamepedia\models\User');
    }

    public function jeux()
    {
        return $this->belongsTo('gamepedia\models\Game');
    }
}
