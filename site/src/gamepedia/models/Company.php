<?php

namespace gamepedia\models;

class Company extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'company';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function jeux()
    {
        return $this->belongsToMany('gamepedia\models\Game', 'game_developers', 'comp_id', 'game_id');
    }
}
