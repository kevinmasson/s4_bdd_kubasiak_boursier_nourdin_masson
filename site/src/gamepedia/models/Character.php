<?php

namespace gamepedia\models;

class Character extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'character';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function game()
    {
        return $this->belongsToMany('gamepedia\models\Game', 'game2character', 'game_id', 'character_id');
    }

}
