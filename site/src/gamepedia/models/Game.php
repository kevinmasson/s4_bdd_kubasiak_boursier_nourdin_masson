<?php

namespace gamepedia\models;

class Game extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'game';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function personnages()
    {
        return $this->belongsToMany('gamepedia\models\Character', 'game2character', 'game_id', 'character_id');
    }

    public function ratings()
    {
        return $this->belongsToMany('gamepedia\models\GameRating', 'game2rating', 'game_id', 'rating_id');
    }

    public function compagniesDeveloppement()
    {
        return $this->belongsToMany('gamepedia\models\Company', 'game_developers', 'game_id', 'comp_id');
    }

    public function compagniesPublication()
    {
        return $this->belongsToMany('gamepedia\models\Company', 'game_publishers', 'game_id', 'comp_id');
    }

    public function commentaires()
    {
        return $this->hasMany('gamepedia\models\Com', 'game_id');
    }
}
