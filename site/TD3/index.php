<h2>TD3</h2>

<ol>
    <li><a href="requete1.php">afficher (name , deck) les personnages du jeu 12342</a></li>
    <li><a href="requete2.php">les personnages des jeux dont le nom (du jeu) débute par 'Mario'</a></li>
    <li><a href="requete3.php">les jeux développés par une compagnie dont le nom contient 'Sony'</a></li>
    <li><a href="requete4.php">le rating initial (indiquer le rating board) des jeux dont le nom contient Mario</a></li>
    <li><a href="requete5.php">les jeux dont le nom débute par Mario et ayant plus de 3 personnages</a></li>
    <li><a href="requete6.php">les jeux dont le nom débute par Mario et dont le rating initial contient "3+"</a></li>
    <li><a href="requete7.php">les jeux dont le nom débute par Mario, publiés par une compagnie dont le nom contient
            "Inc." et dont le rating initial contient "3+"</a></li>
    <li><a href="requete8.php">les jeux dont le nom débute Mario, publiés par une compagnie dont le nom contient "Inc",
            dont le rating initial contient "3+" et ayant reçu un avis de la part du rating board nommé
            "CERO"</a></li>
</ol>
