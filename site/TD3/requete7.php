<?php

require_once('../vendor/autoload.php');

use conf\DbConf;
use gamepedia\models\Game;

DbConf::init('../conf/db.gamepedia.conf.ini');


echo "TD3 <br>";

echo "Requete 5 <br>";

echo "<h2>Jeux dont le nom commence par Mario, dont le nom de la compagnie de publication contient Inc. et qui a été classé 3+ initialement : </h2></br>";

echo "<p><small>";

$t = new gamepedia\Time();
$jeuxMario = Game::where('name', 'LIKE', 'Mario%')->get();
$t->end();

foreach ($jeuxMario as $jeu) {
    foreach ($jeu->compagniesPublication as $compagnie) {
        if (str_contains($compagnie->name, "Inc.")) {
            foreach ($jeu->ratings as $classement) {
                if (str_contains($classement->name, "3+"))
                    echo "<h3>$jeu->name</h3></br>";
            }
        }
    }
}

echo "</small></p>";
