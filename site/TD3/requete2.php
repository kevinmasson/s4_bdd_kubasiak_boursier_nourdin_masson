<?php

require_once('../vendor/autoload.php');

use conf\DbConf;
use gamepedia\models\Game;

DbConf::init('../conf/db.gamepedia.conf.ini');


echo "TD3 <br>";

echo "Requete 2  - Les personnages des jeux dont le nom (du jeu) débute par 'Mario' (name, deck)<br>";

echo "<p>";

$t = new gamepedia\Time();
$lesJeux = Game::where('name', 'LIKE', 'Mario%')->get();
$t->end();

foreach ($lesJeux as $leJeu) {
    echo "<p>";
    echo "<h2>" . $leJeu->name . "</h2>";
    $lesPerso = $leJeu->personnages;

    foreach ($lesPerso as $perso) {
        echo $perso->name . "  -  ";
        echo $perso->deck . "<br>";
    }
    echo "</p>";
}


echo "</p>";
