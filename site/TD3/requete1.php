<?php

require_once('../vendor/autoload.php');


use conf\DbConf;
use gamepedia\models\Game;

DbConf::init('../conf/db.gamepedia.conf.ini');


echo "TD3 <br>";

echo "Requete 1  - Les personnages du jeu 12342 (name, deck)<br>";

echo "<p>";

$t = new gamepedia\Time();
$leJeu = Game::find(12342);

$lesPerso = $leJeu->personnages;
$t->end();

foreach ($lesPerso as $perso) {
    echo $perso->name . "  -  ";
    echo $perso->deck . "<br>";
}


echo "</p>";
