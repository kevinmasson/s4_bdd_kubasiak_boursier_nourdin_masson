<?php

require_once('../vendor/autoload.php');

use conf\DbConf;
use gamepedia\models\Company;


DbConf::init('../conf/db.gamepedia.conf.ini');


echo "TD3 <br>";

echo "Requete 3 - les jeux developpes par une compagnie dont le nom contient 'Sony'<br>";

echo "<p><small>";

$t = new gamepedia\Time();
$comp = Company::where('name', 'LIKE', '%Sony%')->get();
$t->end();
foreach ($comp as $c) {
    echo '<h2>' . $c->name . "</h2><br>";
    $jeux = $c->jeux;
    foreach ($jeux as $j) {
        echo '     ' . $j->name . "<br>";
    }
}

echo "</small></p>";
