<?php

require_once('../vendor/autoload.php');

use conf\DbConf;
use gamepedia\models\Game;

DbConf::init('../conf/db.gamepedia.conf.ini');


echo "TD3 <br>";

echo "Requete 5 <br>";

echo "<h2>Jeux dont le nom commence par Mario et qui ont plus de 3 personnages : </h2></br>";

echo "<p><small>";

$t = new gamepedia\Time();
$jeuxMario = Game::where('name', 'LIKE', 'Mario%')->get();
$t->end();

foreach ($jeuxMario as $jeu) {
    if ($jeu->personnages->count() >= 3) {
        echo "<h3>" . $jeu->name . "</h3>";
    }
}

echo "</small></p>";
