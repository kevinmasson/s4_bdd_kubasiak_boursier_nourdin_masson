<?php

require_once('../vendor/autoload.php');

use conf\DbConf;
use gamepedia\models\Game;

DbConf::init('../conf/db.gamepedia.conf.ini');


echo "TD3 <br>";

echo "Requete 8  - Les jeux dont le nom débute Mario, publiés par une compagnie dont le nom contient 'Inc',
dont le rating initial contient '3+' et ayant reçu un avis de la part du  rating board nommé
'CERO'<br>";

echo "<p>";

$t = new gamepedia\Time();
$lesJeux = Game::where('name', 'LIKE', 'Mario%')
    ->whereHas('compagniesPublication', function ($q) {
        $q->where('name', 'LIKE', '%Inc%');
    }
    )
    ->whereHas('ratings', function ($q) {
        $q->where('name', 'LIKE', "%3+");
    }
    )
    ->whereHas('ratings', function ($q) {
        $q->whereHas('ratingBoard', function ($q2) {
            $q2->where('name', '=', 'CERO');
        }
        );
    }
    )
    ->get();
$t->end();

foreach ($lesJeux as $leJeu) {
    echo "<p>";
    echo "<h2>" . $leJeu->name . "</h2>";
    foreach ($leJeu->compagniesPublication as $cp) {
        echo "Compagnie : " . $cp->name . "<br>";
    }
    echo "<h4>Rating board associés au jeu : </h4><p>";
    foreach ($leJeu->ratings as $r) {
        echo "Rating (" . $r->name . ") par Board : " . $r->ratingBoard->name . '<br>';
    }
    echo "</p></p>";
}


echo "</p>";
