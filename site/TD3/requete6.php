<?php

require_once('../vendor/autoload.php');

use conf\DbConf;
use gamepedia\models\Game;

DbConf::init('../conf/db.gamepedia.conf.ini');


echo "TD3 <br>";

echo "Requete 6  - Les jeux dont le nom debute par Mario et dont le rating initial contient '3+'
<br>";

echo "<p>";

$t = new gamepedia\Time();
$jeux = Game::where('name', 'LIKE', 'Mario%')->get();
$t->end();

foreach ($jeux as $j) {
    $rat = $j->ratings->take(1)->where('name', 'LIKE', '%3+%');
    if (!is_null($rat)) {
        echo $j->name . '<br>';
    }
}


echo "</p>";
