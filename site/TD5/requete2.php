<?php

require_once('../vendor/autoload.php');

use conf\DbConf;
use gamepedia\models\Game;

DbConf::init('../conf/db.gamepedia.conf.ini');


echo "TD5 <br>";

echo "Requete 1 <br>";

echo "<h2>Lister les jeux dont le nom contient mario</h2></br>";

echo "<p><small>";

$t = new gamepedia\Time();
$jeuxMario = Game::where('name', 'LIKE', '%Mario%')->get();
$t->end();

foreach ($jeuxMario as $jeu) {
    echo "<h3>" . $jeu->name . "</h3>";
}

echo "</small></p>";
