<?php

require_once('../vendor/autoload.php');

use conf\DbConf;
use gamepedia\models\Game;
use Illuminate\Database\Capsule\Manager as DB;

DbConf::init('../conf/db.gamepedia.conf.ini');
DB::enableQueryLog();

echo "TD5 <br>";

echo "Requete 1 <br>";

/*
* CREATE InDEX idx_game_name on game(name);
Question 2: un index sur compagnie(nom)
*/

echo "<h2>Lister les jeux dont le nom debute par mario (5secondes contre 0.7s avec indexage)</h2></br>";

echo "<p><small>";

$t = new gamepedia\Time();
$jeuxMario = Game::where('name', 'LIKE', 'Mario%')->get();
$t->end();


foreach ($jeuxMario as $jeu) {
    echo "<h3>" . $jeu->name . "</h3>";
}


echo "</small></p>";


$query = DB::getQueryLog();
var_dump($query);
