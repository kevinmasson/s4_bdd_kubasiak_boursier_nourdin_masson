<h2>TD5</h2>

<ol>
    <li><a href="requete1.php">Les jeux dont le nom debute par mario</a></li>
    <li><a href="requete2.php">Les jeux dont le nom contient mario</a></li>
    <li><a href="requete3.php">Recherche sur les noms des compagnies</a></li>
    <li><a href="requete4.php">Recherche sur les pays des compagnies</a></li>
    <li><a href="requete5.php">Eager load personnages des jeux dont le nom contient mario</a></li>
    <li><a href="requete5bis.php">Personnages des jeux dont le nom contient mario</a></li>
    <li><a href="requete6.php">Eager load des jeux développés par une compagnie dont le nom contient Sony</a></li>
</ol>
