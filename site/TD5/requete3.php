<?php

require_once('../vendor/autoload.php');

use conf\DbConf;
use gamepedia\models\Company;

DbConf::init('../conf/db.gamepedia.conf.ini');

echo "TD5 <br>";

echo "Requete 4 <br>";

echo "<h2>Recherche sur le nom d'une compagnie: 0.02s sans index, 0.00119s avec index sur name</h2></br>";

echo "<p><small>";


$t = new gamepedia\Time();
$names = Company::where('name', 'LIKE', 'Electronic%')->get();
$t->end();


foreach ($names as $name) {
    echo "<h3>" . $name->name . "</h3>";
}

echo "</small></p>";
