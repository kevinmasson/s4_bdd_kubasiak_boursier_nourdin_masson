<?php

require_once('../vendor/autoload.php');

use conf\DbConf;
use gamepedia\models\Company;

DbConf::init('../conf/db.gamepedia.conf.ini');


echo "TD5 <br>";

echo "Requete 4 <br>";

echo "<h2>Recherche sur le pays d'une compagnie: 0.04s sans index, 0.03s avec index sur location_country</h2></br>";

echo "<p><small>";

$t = new gamepedia\Time();
$countries = Company::where('location_country', 'LIKE', 'U%')->get();
$t->end();


foreach ($countries as $country) {
    echo "<h3>" . $country->location_country . "</h3>";
}

echo "</small></p>";
