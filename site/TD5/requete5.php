<?php

require_once('../vendor/autoload.php');

use conf\DbConf;
use gamepedia\models\Game;

DbConf::init('../conf/db.gamepedia.conf.ini');

echo "TD5 <br>";

echo "Requete 6 <br>";

echo "<h2>Eager load des personnages des jeux dont le nom contient mario</h2></br>";

echo "<p><small>";

$t = new gamepedia\Time();
$chars = Game::where('name', 'like', '%Mario%')->with('personnages')->get();
$t->end();
foreach ($chars as $c) {
    echo $c->name . "<br>";
}

echo "</small></p>";
